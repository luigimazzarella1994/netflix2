//
//  AnteprimaCollectionViewController.swift
//  Netflix2
//
//  Created by Luigi Mazzaerella on 12/12/2019.
//  Copyright © 2019 Luigi Mazzaerella. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class AnteprimaCollectionViewController: UICollectionViewController {
    
    
    var arrayImmagini:  [UIImage] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if CheckInternet.Connection(){
            
                self.dataImage()
            
            
        }
        else {
            collectionView.isHidden = true
        }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }
    
    func dataImage() {
        arrayImmagini.append(UIImage(named: "finalSpace")!)
        arrayImmagini.append(UIImage(named: "klaus")!)
        arrayImmagini.append(UIImage(named: "peaky")!)
        arrayImmagini.append(UIImage(named: "piovono")!)
        arrayImmagini.append(UIImage(named: "daybreak")!)
        arrayImmagini.append(UIImage(named: "jumanji")!)
        arrayImmagini.append(UIImage(named: "vwars")!)
        arrayImmagini.append(UIImage(named: "theEnd")!)
        
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return arrayImmagini.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "anteprima", for: indexPath) as! ComingCollectionViewCell
    
        // Configure the cell
        
        cell.coverComing.image = arrayImmagini[indexPath.row]
    
        return cell
    }


}
