//
//  ShowViewController.swift
//  Netflix2
//
//  Created by Luigi Mazzaerella on 15/12/2019.
//  Copyright © 2019 Luigi Mazzaerella. All rights reserved.
//

import UIKit

class ShowViewController: UIViewController {
    
    @IBOutlet weak var cover: UIImageView!
    @IBOutlet weak var titleFilm: UILabel!
    @IBOutlet weak var categoryFilm: UILabel!
    
    var image: UIImage!
    var titleOfCell = ""
    var category: [String] = []
    var filmSimili: [UIImage] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cover.image = image
        titleFilm.text = titleOfCell
        categoryFilm.text = category.joined(separator: ", ")
        
        
    }
    

}

extension ShowViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filmSimili.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "similar", for: indexPath) as! ModalCollectionViewCell
        
        cell.coverModal.image = filmSimili[indexPath.row]
        
        return cell
    }
    
    
}
