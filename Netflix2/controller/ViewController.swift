//
//  ViewController.swift
//  Netflix
//
//  Created by Luigi Mazzaerella on 04/12/2019.
//  Copyright © 2019 Luigi Mazzaerella. All rights reserved.
//

import UIKit

let fontUrl = Bundle.main.url(forResource: "SultanNahiaW20", withExtension: "ttf")
var font = CTFontManagerRegisterFontsForURL(fontUrl! as CFURL, CTFontManagerScope.process, nil)
let fontOK = UIFont(name: "SultanNahiaW20", size: 22)

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
   
    

    @IBOutlet weak var firstImage: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var viewComing: UIView!
    @IBOutlet weak var viewLoad: UIView!
    @IBOutlet weak var imageLoad: UIImageView!
    
    var storedOffsets = [Int: CGFloat]()
    var category: [String] = []
    var element = ListElement(itemListElement: [])
    var filmCategory: [[ItemList]] = []
    var Immagini: [String:UIImage] = [:]
    var Immagini2: [String: UIImage] = [:]
    var name: String = ""
    
   
    @IBAction func infoButton(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(identifier: "ShowViewController") as! ShowViewController
        var categories: [String] = []
        
        vc.image = Immagini[name]
        vc.titleOfCell = name
        
        for elem in element.itemListElement{
            
            if name == elem.item.name {
                vc.category = elem.item.category
                categories = elem.item.category
                
            }
        }
        
        for elem in element.itemListElement {
            
            for categoria in elem.item.category {
                
                for categorie in categories{
                    if categorie == categoria {
                        for (names, cover) in Immagini2{
                            if names != name {
                                if !vc.filmSimili.contains(cover) {
                                    vc.filmSimili.append(cover)
                                }
                            }
                        }
                    }
                }
            }
            
        }
        
        
        
       
        
        self.present(vc, animated: true)
       
                       
}


    
    override func viewDidLoad() {
        myNavigationBar()
        infoButton.isHidden = true
        categoryLabel.isHidden = true
        table.isHidden = true
        if CheckInternet.Connection(){
            UIImageView.animate(withDuration: 2.0, delay: 0.0, options: [.repeat, .curveLinear, .beginFromCurrentState, .autoreverse], animations: {
                self.imageLoad.frame.size.height = 20
                self.imageLoad.frame.size.width = 20
            })
        }
        
        
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
         
        
          if CheckInternet.Connection(){
            
            
            loadData()
            loadImage()
            DispatchQueue.main.async {
                
                self.viewLoad.isHidden = true
                self.viewComing.isHidden = false
                self.imageLoad.isHidden = true
                self.categoryOfFilm()
                self.filmPerCategory()
                self.pushImage()
                self.infoButton.isHidden = false
                self.categoryLabel.isHidden = false
                self.table.isHidden = false
                self.table.reloadData()
                
            }
            
              
          }
          
          else{
            let alert = UIAlertController(title: "Impossible to connect ", message: "It's recommended to verify your internet connection.", preferredStyle: .alert)
            
            
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: {
                (action: UIAlertAction) in
                self.viewDidAppear(true)
            }))
            
            self.present(alert, animated: true)
            categoryLabel.isHidden = true
            infoButton.isHidden = true
            
        }
          
      }
    
    func pushImage(){
 
        let item = element.itemListElement[Int.random(in: 0 ..< Immagini2.count)]
        name = item.item.name
        firstImage?.image = Immagini2[name]
        categoryLabel.text = item.item.category.joined(separator: " • " )
        categoryLabel.textColor = .white
  
        
    }
    
    
    
    func loadImage()  {
        
         
            let url = Bundle.main.url(forResource: "netflixDataJson", withExtension: "json")!
            
            
            do {
                let data = try Data(contentsOf: url)
                let decoder: JSONDecoder = JSONDecoder.init()
                let  user: ListElement = try decoder.decode(ListElement.self, from: data )
                
                self.element = user
                
                for elem in user.itemListElement {
                    let url = elem.item.url
                    do {
                        let data = try Data(contentsOf: url )
                        
                        //4. Make image
                        let  image = UIImage(data: data)
                        self.Immagini[elem.item.name] = image
                        
                        let url2 = elem.item.url2
                        do {
                            let data = try Data(contentsOf: url2 )
                            
                            //4. Make image
                            let  image = UIImage(data: data)
                            self.Immagini2[elem.item.name] = image
                            
                            
                        } catch let e {
                            print(e)
                        }
                        
                    } catch let e {
                        print(e)
                        
                        
                    }
                }
                
            } catch let e {
                print(e)
                
            }
            
  
        
    }
    
 func categoryOfFilm(){
           for film in element.itemListElement {
               for categoryInFilm in film.item.category {
                   if !category.contains(categoryInFilm){
                       category.append(categoryInFilm)
                       print("Ci sono queste categorie \(categoryInFilm)")
                   }
               }
           }
       }
    
    func filmPerCategory(){

          var category2: [String] = []
          var arrTemp: [ItemList] =  []

        for film in element.itemListElement {
            for categoryInFilm in film.item.category {
                  if !category2.contains(categoryInFilm){
                      category2.append(categoryInFilm)
                  }
              }
          }

          for categoria in category2 {
              arrTemp.removeAll()
            for film in element.itemListElement {
                for categories in film.item.category {
                      if categories == categoria  {
                          arrTemp.append(film)
                      }
                  }
              }
              filmCategory.append(arrTemp)
          }
        
        var count = 0
        while count < filmCategory.count {
            print("Sono filmCategory array num \(count) e contengo \(filmCategory[count])")
            count += 1
        }

      }
    
    func myNavigationBar(){
        
        
        
        self.navigationController?.hidesBarsOnSwipe = true
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.isTranslucent = true
        
        var button: [UIBarButtonItem] = []
        

        let logoView = UIButton(type: .custom)
        let image = UIImage(named: "logo")
        logoView.setImage(image, for: .normal)
        logoView.widthAnchor.constraint(equalToConstant: 45.0).isActive = true
        logoView.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        logoView.contentMode = .scaleAspectFit
        
        let serieLabel = UILabel()
        serieLabel.text = "      Serie TV     "
        serieLabel.textColor = .white
        serieLabel.font = fontOK
       
        let miaListaLabel = UILabel()
        miaListaLabel.text = "La mia lista     "
        miaListaLabel.textColor = .white
        miaListaLabel.font = fontOK
        
        let filmLabel = UILabel()
        filmLabel.text = "Film     "
        filmLabel.textColor = .white
        filmLabel.font = fontOK


        
        let serie = UIBarButtonItem(customView: serieLabel )
        let miaLIsta = UIBarButtonItem(customView: miaListaLabel )
        let logo = UIBarButtonItem(customView: logoView)
        let film = UIBarButtonItem(customView: filmLabel )
       
        
        button.append(logo)
        button.append(serie)
        button.append(film)
        button.append(miaLIsta)
        
        navigationItem.leftBarButtonItems = button
        
        
        
       
    }
    
     func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return category.count
       }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "category", for: indexPath) as! CategoryTableViewCell
        cell.categoryName.text = category[indexPath.row]
        cell.categoryName.font = fontOK
        cell.categoryName.textColor  = .white
        return cell
    }
    
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let categoryTableViewCell = cell as? CategoryTableViewCell else {return}
        categoryTableViewCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
        categoryTableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
     func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let categoryTableViewCell = cell as? CategoryTableViewCell else {return}
        categoryTableViewCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
        storedOffsets[indexPath.row] = categoryTableViewCell.collectionViewOffset
    }
    
    func loadData()  {
        let url = Bundle.main.url(forResource: "netflixDataJson", withExtension: "json")!

                
                do {
                    let data = try Data(contentsOf: url)
                    let decoder: JSONDecoder = JSONDecoder.init()
                  let  user: ListElement = try decoder.decode(ListElement.self, from: data )
                    
                    element = user
        //            print("Ciao, \(user)")
                    
                    for elem in user.itemListElement {
                        print("sono di tipo \(elem.item.type) e mi chiamo \(elem.item.name)")
                    }
                    
                } catch let e {
                    print(e)
                }
            }
    }
  



extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
   
    
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("filmCategory ha \(filmCategory[collectionView.tag].count) elementi")
        return filmCategory[collectionView.tag].count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "film", for: indexPath) as! FilmCollectionViewCell

//        cell.coverFilm.image = filmCategory[collectionView.tag][indexPath.item].image
       
       
//        let url = filmCategory[collectionView.tag][indexPath.item].item.url
        
        for (name, cover) in Immagini {
            if name == filmCategory[collectionView.tag][indexPath.item].item.name {
                cell.coverFilm.image = cover
                break
            }
        }
     
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(identifier: "ShowViewController") as! ShowViewController
        
        for (name, cover) in Immagini {
            if name == filmCategory[collectionView.tag][indexPath.item].item.name {
                vc.image = cover
                vc.titleOfCell = filmCategory[collectionView.tag][indexPath.item].item.name
                vc.category = filmCategory[collectionView.tag][indexPath.item].item.category
                break
            }
            
            for elem in element.itemListElement {
                
                for categoria in elem.item.category {
                    for categorie in filmCategory[collectionView.tag][indexPath.item].item.category {
                        if categoria == categorie {
                            for (name, cover) in Immagini {
                                if name != filmCategory[collectionView.tag][indexPath.row].item.name {
                                    if name == elem.item.name {
                                        if !vc.filmSimili.contains(cover) {
                                            vc.filmSimili.append(cover)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
            }
        }
        self.present(vc, animated: true)
    }
    
    
}



