//
//  Film.swift
//  Netflix
//
//  Created by Luigi Mazzaerella on 04/12/2019.
//  Copyright © 2019 Luigi Mazzaerella. All rights reserved.
//

import Foundation
import UIKit

struct ListElement: Codable {
    let itemListElement: [ItemList]
}

struct ItemList: Codable {
    let item: Item
}

struct Item: Codable {
    
    let type: String
    let name: String
    let category: [String]
    let url: URL
    let url2: URL
    
}
