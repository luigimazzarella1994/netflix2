//
//  CategoryTableViewCell.swift
//  Netflix
//
//  Created by Luigi Mazzaerella on 06/12/2019.
//  Copyright © 2019 Luigi Mazzaerella. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet private weak var collectionView: UICollectionView!
  
   
    
    var collectionViewOffset: CGFloat {
        get {
            return collectionView.contentOffset.x
        }

        set {
            collectionView.contentOffset.x = newValue
        }
    }
}

extension CategoryTableViewCell {
   
    func setCollectionViewDataSourceDelegate(dataSourceDelegate: UICollectionViewDataSource & UICollectionViewDelegate, forRow row: Int) {
           collectionView.delegate = dataSourceDelegate
           collectionView.dataSource = dataSourceDelegate
           collectionView.tag = row
           collectionView.reloadData()
       }
}
