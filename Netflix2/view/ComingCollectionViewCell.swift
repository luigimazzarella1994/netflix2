//
//  ComingCollectionViewCell.swift
//  Netflix2
//
//  Created by Luigi Mazzaerella on 12/12/2019.
//  Copyright © 2019 Luigi Mazzaerella. All rights reserved.
//

import UIKit

class ComingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var coverComing: UIImageView!
}
