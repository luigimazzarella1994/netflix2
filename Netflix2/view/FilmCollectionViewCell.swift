//
//  FilmCollectionViewCell.swift
//  Netflix
//
//  Created by Luigi Mazzaerella on 06/12/2019.
//  Copyright © 2019 Luigi Mazzaerella. All rights reserved.
//

import UIKit

class FilmCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var coverFilm: UIImageView!
}
