//
//  Gradient.swift
//  Netflix2
//
//  Created by Luigi Mazzaerella on 15/12/2019.
//  Copyright © 2019 Luigi Mazzaerella. All rights reserved.
//

import UIKit

class Gradient: UIImageView {

   let myGradientLayer: CAGradientLayer
   
   override init(frame: CGRect){
       myGradientLayer = CAGradientLayer()
       super.init(frame: frame)
       self.setup()
       addGradientLayer()
   }
   
   func addGradientLayer(){
       if myGradientLayer.superlayer == nil{
           self.layer.addSublayer(myGradientLayer)
       }
   }
   
   required init(coder aDecoder: NSCoder){
       myGradientLayer = CAGradientLayer()
       super.init(coder: aDecoder)!
       self.setup()
       addGradientLayer()
   }
   
   func getColors() -> [CGColor] {
       return [UIColor.clear.cgColor, UIColor(red: 25/255.0, green: 25/255.0, blue: 25/255.0, alpha: 1).cgColor]
   }
   
   func getLocations() -> [CGFloat]{
       return [0.4,  0.9]
   }
   
   func setup() {
       myGradientLayer.startPoint = CGPoint(x: 0.6, y: 0)
       myGradientLayer.endPoint = CGPoint(x: 0.6, y: 1)
       
       let colors = getColors()
       myGradientLayer.colors = colors
       myGradientLayer.isOpaque = false
       myGradientLayer.locations = getLocations() as [NSNumber]?
   }
   
   override func layoutSubviews() {
       super.layoutSubviews()
       myGradientLayer.frame = self.layer.bounds
   }

}
