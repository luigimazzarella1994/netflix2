//
//  ModalCollectionViewCell.swift
//  Netflix2
//
//  Created by Luigi Mazzaerella on 15/12/2019.
//  Copyright © 2019 Luigi Mazzaerella. All rights reserved.
//

import UIKit

class ModalCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var coverModal: UIImageView!
}
